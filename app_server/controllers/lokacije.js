/* Vrni začetno stran s seznamom lokacij */
module.exports.seznam = function(req, res) {
  res.render('lokacije-seznam', { 
    title: 'EduGeoCache - Poiščite zanimive lokacije blizu vas!', 
    glavaStrani: {
      naslov: 'EduGeoCache',
      podnaslov: 'Poiščite zanimive lokacije blizu vas!',
    },
    stranskaOrodnaVrstica: 'Iščete lokacijo za kratkočasenje? EduGeoCache vam pomaga pri iskanju zanimivih lokacij v bližini. Mogoče imate kakšne posebne želje? Naj vam EduGeoCache pomaga pri iskanju bližnjih zanimivih lokacij.',
    lokacije: [{
      naziv: 'Stari grad Celje',
      naslov: 'Cesta na grad 78, 3000 Celje, Slovenija',
      ocena: 3,
      lastnosti: [ 'slikovit razgled', 'vstopnina', 'otrokom prijazno' ],
      razdalja: '700m'
    }, {
      naziv: 'ZOO Ljubljana',
      naslov: 'Večna pot 70, 1000 Ljubljana, Slovenia',
      ocena: 4,
      lastnosti: [ 'priporočljivo za otroke', 'živali', 'parkirišče je na voljo', 'vstopnina' ],
      razdalja: '85km'
    }]
  });
};

/* Vrni začetno stran s seznamom lokacij */
module.exports.podrobnostiLokacije = function(req, res) {
  res.render('lokacija-podrobnosti', { 
    title: 'Stari grad Celje', 
    glavaStrani: {
      naslov: 'Stari grad Celje'
    },
    stranskaOrodnaVrstica: {
      kontekst: 'je na EduGeoCache ker je zanimiva lokacija, ki si jo lahko ogledate, ko ste brez idej za kratek izlet.',
      poziv: 'Če vam je lokacija všeč, ali pa tudi ne, dodajte svoj komentar in s tem pomagajte ostalim uporabnikom pri odločitvi.'
    },
    lokacija: {
      naziv: 'Stari grad Celje',
      naslov: 'Cesta na grad 78, 3000 Celje, Slovenija',
      ocena: 3,
      lastnosti: [ 'slikovit razgled', 'vstopnina', 'otrokom prijazno' ],
      koordinate: {lat: 46.219849, lng: 15.271601},
      delovniCas: [{
        dnevi: 'ponedeljek - petek',
        odprtje: '9:00',
        zaprtje: '21:00',
        zaprto: false
      }, {
        dnevi: 'sobota',
        odprtje: '9:00',
        zaprtje: '19:00',
        zaprto: false
      }, {
        dnevi: 'nedelja',
        zaprto: true
      }],
      komentarji: [{
        avtor: 'Dejan Lavbič',
        ocena: 5,
        datum: '12. november, 2017',
        besediloKomentarja: 'Odlična lokacija, ne morem je prehvaliti.'
      }, {
        avtor: 'Kim Jong Un',
        ocena: 1,
        datum: '12. november, 2017',
        besediloKomentarja: 'Čisti dolgčas, še kava je zanič.'
      }]
    }
  });
};

/* Vrni stran za dodajanje komentarjev */
module.exports.dodajKomentar = function(req, res) {
  res.render('lokacija-nov-komentar', { 
    title: 'Dodaj komentar za Stari grad Celje na EduGeoCache',
    glavaStrani: {
      naslov: 'Komentiraj Stari grad Celje'
    }
  });
}