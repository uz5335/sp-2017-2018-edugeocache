var express = require('express');
var router = express.Router();

var ctrlLokacije = require('../controllers/lokacije.js');
var ctrlOstalo = require('../controllers/ostalo.js');

/* Lokacijske strani */
router.get('/', ctrlLokacije.seznam);
router.get('/lokacija', ctrlLokacije.podrobnostiLokacije);
router.get('/lokacija/komentar/nov', ctrlLokacije.dodajKomentar);

/* Ostale strani */
router.get('/informacije', ctrlOstalo.informacije);

module.exports = router;
